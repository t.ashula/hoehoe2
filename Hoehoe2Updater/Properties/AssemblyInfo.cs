using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("TweenUp")]
[assembly: AssemblyDescription("Tween Updater")]
[assembly: AssemblyCompany("Tween")]
[assembly: AssemblyProduct("TweenUp")]
[assembly: AssemblyCopyright("(C) kiri_feather 2007 - 2011")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("f49350f8-90e7-4f0e-9a05-569e25f8e335")]
[assembly: AssemblyVersion("1.0.0.13")]
[assembly: AssemblyFileVersion("1.0.0.13")]